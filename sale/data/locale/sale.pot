# Translations template for sale.
# Copyright (C) 2018 Tryvy Sale
# This file is distributed under the same license as the sale project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: sale 1.0\n"
"Report-Msgid-Bugs-To: info@datalifeit.es\n"
"POT-Creation-Date: 2018-06-22 15:33+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: sale/app.py:50 sale/sale.kv:8 sale/sale_module.kv:25 sale/sale_module.kv:37
msgid "Sale"
msgstr ""

#: sale/sale_module.kv:44
msgid "Sale Lines"
msgstr ""

#: sale/sale_module.kv:54
msgid "Sale extra information"
msgstr ""

#: sale/sale_module.py:86 sale/sale_module.py:98
msgid "Server error!"
msgstr ""

#: sale/sale_module.py:87
msgid "Cannot Quote Sale!"
msgstr ""

#: sale/sale_module.py:99
msgid "Cannot back to Draft Sale!"
msgstr ""

